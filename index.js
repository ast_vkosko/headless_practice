import Koa from 'koa';
import Apollo from 'apollo-server-koa';
import CommerceSDK from 'commerce-sdk';
import dotenv from 'dotenv';
import typeDefs from './entities/typeDefs.js';
import resolvers from './entities/resolvers.js';
import Headless from './headless.js';

dotenv.config();

const { ApolloServer } = Apollo;
const app = new Koa();
const headless = new Headless();
const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ ctx }) => ({
        getClientConfig: async () => {
            let { token } = await headless.getToken();
            let clientConfig = headless.config;

            clientConfig.headers.authorization = token;

            return clientConfig;
        },
        ctx
    })
});

app.use(server.getMiddleware({ path: process.env.APOLLO_PATH }));
app.listen({ port: process.env.PORT || '3000' }, async () => {
    console.log(`Apollo Address: http://localhost:${process.env.PORT}${process.env.APOLLO_PATH}`);
    CommerceSDK.sdkLogger.setLevel(CommerceSDK.sdkLogger.levels.INFO);
});
