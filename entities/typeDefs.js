import Apollo from 'apollo-server-koa';

const { gql } = Apollo;

const typeDefs = gql`
    type Query {
        productSearch(query: String!): SearchResult
    }

    type SearchResult {
        limit: Int
        hits: [ProductHit]
    }

    type ProductHit {
        productId: ID!
        productName: String!
        price: Float
        currency: String
    }
`;

export default typeDefs;
